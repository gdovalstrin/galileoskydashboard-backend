const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
var app = express();

const { MongoClient } = require("mongodb");

const localUri = "mongodb://localhost:27017";


const uri = localUri;
const dbName = "galileoData";

console.log(uri);

async function findAll(dates={from: false, to: false}) {

  const client = new MongoClient(uri);
  await client.connect();

  const collectionName = "positions";

  const database = client.db(dbName);
  const collection = database.collection(collectionName);

  let query = {};
  if (dates.from && dates.to)
  {
    query = {
      date_time: {
        $gte: dates.from,
        $lte: dates.to
      }
    };
  }
  let positions = [];
  try {
    const cursor = await collection.find(query).sort({ date_time: -1 }).limit(1000).toArray();
    await cursor.forEach(position => {
      //console.log(position);
      positions.push(position);
    });
    // add a linebreak
    console.log();
  } catch (err) {
    console.error(`Something went wrong trying to find the documents: ${err}\n`);
  }
  // Make sure to call close() on your client to perform cleanup operations
  await client.close();
  return await positions;
}

async function findAllEvents(dates={from: false, to: false}) {
  
    const client = new MongoClient(uri);
    await client.connect();

    const collectionName = "positions";
  
    const database = client.db(dbName);
    const collection = database.collection(collectionName);
  
    let query = {
        $expr: {
            $ne: [{ $arrayElemAt: ["$usertag_0", 0] }, 0]
        }
    }
    if (dates.from && dates.to)
    {
      query = {
        $and: [
            { usertag_0: { $ne: [0] } },
            { date_time: {
                $gte: dates.from,
                $lte: dates.to
              }
            }
        ]
      }
    }
    let events = [];
    try {
      const cursor = await collection.find(query).sort({insert_datetime: -1, date_time: -1}).limit(1000).toArray();
      await cursor.forEach(event => {
        //console.log(event);
        events.push(event);
      });
      // add a linebreak
      console.log();
    } catch (err) {
      console.error(`Something went wrong trying to find the documents: ${err}\n`);
    }
    // Make sure to call close() on your client to perform cleanup operations
    await client.close();
    return await events;
}

async function findAllEventsByImei(imei, dates={from: false, to: false}) {
  
    const client = new MongoClient(uri);
    await client.connect();
  
    const collectionName = "positions";
  
    const database = client.db(dbName);
    const collection = database.collection(collectionName);
  
    let query = {
      usertag_0: { $elemMatch: { $ne: 0 } },
      imei: imei
    }
    if (dates.from && dates.to)
    {
      query = {
        usertag_0: { $ne: [0] },
        imei: imei,
        date_time: {
          $gte: dates.from,
          $lte: dates.to
        }
      }
    }
    let events = [];
    try {
      const cursor = await collection.find(query, {sort: {insert_datetime: -1, date_time: -1}}).limit(1000).toArray();
      await cursor.forEach(event => {
        events.push(event);
      });
    } catch (err) {
      console.error(`Something went wrong trying to find the documents: ${err}\n`);
    }
    await client.close();
    return await events;
}

async function findByImei(imei, dates={from: false, to: false}) {
  
    const client = new MongoClient(uri);
    await client.connect();
  
    const collectionName = "positions";
  
    const database = client.db(dbName);
    const collection = database.collection(collectionName);
  
    let query = {
      imei: imei
    };
    if (dates.from && dates.to)
    {
      query.date_time = {
        $gte: dates.from,
        $lte: dates.to
      };
    }
    let positions = [];
    try {
      const cursor = await collection.find(query).sort({insert_datetime: -1, date_time: -1}).limit(10000).toArray();
      await cursor.forEach(position => {
        //console.log(position);
        positions.push(position);
      });
      // add a linebreak
      console.log();
    } catch (err) {
      console.error(`Something went wrong trying to find the documents: ${err}\n`);
    }
    // Make sure to call close() on your client to perform cleanup operations
    await client.close();
    return await positions;
}

async function findLastByImei(imei) {

  const client = new MongoClient(uri);
  await client.connect();

  const collectionName = "positions";

  const database = client.db(dbName);
  const collection = database.collection(collectionName);

  let query = {
    imei: imei
  };
  let sort = {
    sort: {
      insert_datetime: -1,
      date_time: -1
    }
  };
  let position;
  try {
    position = await collection.findOne(query, sort);
    // add a linebreak
    console.log();
  } catch (err) {
    console.error(`Something went wrong trying to find the documents: ${err}\n`);
  }
  // Make sure to call close() on your client to perform cleanup operations
  await client.close();
  if (position != undefined){
    console.log("Se encontro registro:");
    console.log(position);
  } else {
    console.log("No se encontro registro");
  }
  return await position;
}

async function findRegisteredDevices() {
  
    const client = new MongoClient(uri);
    await client.connect();
  
    const collectionName = "positions";
  
    const database = client.db(dbName);
    const collection = database.collection(collectionName);
  
    let devices = [];
    try {
      const cursor = await collection.aggregate([
        {
            $addFields: {
              date_time: { $toDate: "$date_time" },  // Convierte el campo datetime a tipo Date,
              insert_datetime: { $toDate: "$insert_datetime" }
            }
        },
        {
          $group: {
            _id: "$imei",  // Agrupa por el campo imei
            ultimoRegistro: { $max: { $max: ["$date_time", "$insert_datetime"] } },  // Encuentra el registro más nuevo
            ultimoCoord: { $last: "$coord" },       // Obtén el último valor de coord en el grupo
            ultimaTensionBat: { $last: "$tension_bateria"},
            ultimaTensionAli: { $last: "$tension_alimentacion"},
            ultimoEstadoIgn: { $last: "$in_0"},
            documento: { $first: "$$ROOT" }         // Conserva el documento completo del registro más nuevo
          }
        },
        {
          $project: {
            _id: 0,
            imei: "$_id",
            date_time: "$ultimoRegistro",  // Agrega el campo datetime
            coord: "$ultimoCoord",  // Agrega el campo coord
            tension_bateria: "$ultimaTensionBat",
            tension_alimentacion: "$ultimaTensionAli",
            estado_ignicion: "$ultimoEstadoIgn",
            ultimoRegistro: 1,
            documento: 1
          }
        },
        {
          $sort: {
            date_time: -1,
            insert_datetime: -1
          }
        }
      ]).toArray();
      await cursor.forEach(device => {
        //console.log(device);
        new_device = {
            imei: device.imei,
            last_connection: device.date_time,
            last_position: device.coord,
            tension_bateria: device.tension_bateria,
            tension_alimentacion: device.tension_alimentacion,
            estado_ignicion: device.estado_ignicion
        }
        devices.push(new_device);
      });
      //console.log();
    } catch (err) {
      console.error(`Something went wrong trying to find the documents: ${err}\n`);
    }
    await client.close();
    console.log(devices);
    return await devices;
}

async function findRegisteredDeviceByImei(imei) {
  
    const client = new MongoClient(uri);
    await client.connect();
  
    const collectionName = "positions";
  
    const database = client.db(dbName);
    const collection = database.collection(collectionName);
  
    let device;
    try {
      const cursor = await collection.aggregate([
        {
            $match: {
              imei: imei
            }
        },
        {
            $addFields: {
              date_time: { $toDate: "$date_time" }  // Convierte el campo datetime a tipo Date
            }
        },
        {
          $group: {
            _id: "$imei",  // Agrupa por el campo imei
            ultimoRegistro: { $max: "$date_time" },  // Encuentra el registro más nuevo
            ultimoCoord: { $last: "$coord" },       // Obtén el último valor de coord en el grupo
            documento: { $first: "$$ROOT" }         // Conserva el documento completo del registro más nuevo
          }
        },
        {
          $project: {
            _id: 0,
            imei: "$_id",
            date_time: "$ultimoRegistro",  // Agrega el campo datetime
            coord: "$ultimoCoord",  // Agrega el campo coord
            ultimoRegistro: 1,
            documento: 1
          }
        }
      ]).toArray();
      device = cursor[0];
      console.log();
    } catch (err) {
      console.error(`Something went wrong trying to find the documents: ${err}\n`);
    }
    await client.close();
    return await device;
}



// API

app.use(bodyParser.json());
app.use(cors());

app.get("/positions", async (req, res) => {
    let dates = {
      from: req.query.from != null ? req.query.from : false,
      to: req.query.to != null ? req.query.to : false
    };
    console.log(dates);
    res.status(200).send(await findAll(dates).catch(console.dir));
});

app.get("/positions/:id", async (req, res) => {
    let dates = {
      from: req.query.from != null ? req.query.from : false,
      to: req.query.to != null ? req.query.to : false
    };
    console.log(dates);
    res.send(await findByImei(req.params.id, dates).catch(console.dir));
});

app.get("/positions/last/:id", async (req, res) => {
  res.send(await findLastByImei(req.params.id).catch(console.dir));
});

app.get("/events", async (req, res) => {
  let dates = {
    from: req.query.from != null ? req.query.from : false,
    to: req.query.to != null ? req.query.to : false
  };
  res.send(await findAllEvents(dates).catch(console.dir));
}); 

app.get("/events/:id", async (req, res) => {
  let dates = {
    from: req.query.from != null ? req.query.from : false,
    to: req.query.to != null ? req.query.to : false
  };
  res.send(await findAllEventsByImei(req.params.id, dates).catch(console.dir));
}); 

app.get("/devices", async (req, res) => {
  console.log(req.query);
  console.log(req.body);
  res.status(200).json(await findRegisteredDevices(req.params.id).catch(console.dir));
});

app.get("/devices/:id", async (req, res) => {
    res.send(await findRegisteredDeviceByImei(req.params.id).catch(console.dir));
});

app.listen(3001, () => {
    console.log("Listen on port 3001...");
});